(function() {

	const maxTries = 10;
	let tries = 0;

	let removeCommentSection = function() {

		setTimeout(function() {

			tries++;
			let commentSection = document.getElementById('comments');

			if (commentSection) {

				commentSection.innerHTML = "You don't have to read other people's opinions dude come on,...";
			} else if (tries < maxTries) {

				removeCommentSection();
			} else {

				console.log("YOUTUBE-COMMENT-BLOCKER extension says: There is probably no comment section whatsoever.");
			}

		}, 1000);
	}

	removeCommentSection();
})();